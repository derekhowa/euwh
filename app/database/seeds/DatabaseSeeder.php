<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		//$this->call('UserTableSeeder');
        //$this->call('ServiceTableSeeder');
        //$this->call('ProgramTableSeeder');
        //$this->call('KnowTableSeeder');
        //$this->call('GeneticTableSeeder');
        //$this->call('FormTableSeeder');
        //$this->call('DirectionTableSeeder');
        $this->call('HomeTableSeeder');

	}


}

class ServiceTableSeeder extends Seeder {
    public function run()
    {
        Service::create(array(
            'id' => 1,
            'title' => 'subtitle',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
         Service::create(array(
            'id' => 2,
            'title' => 'main',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
    }
}
/**
class ProgramTableSeeder extends Seeder {
    public function run()
    {
        Program::create(array(
            'id' => 1,
            'title' => 'subtitle',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
          Program::create(array(
            'id' => 2,
            'title' => 'main',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
    }
}

class UserTableSeeder extends Seeder {

    public function run()
    {


        User::create(array(
                'id' => 4,
                'username' => 'seconduser',
                'email' => 'email@email.com',
                'password' => Hash::make('second_password'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ));
    }

}


class KnowTableSeeder extends Seeder {
    public function run()
    {
        Know::create(array(
            'id' => 1,
            'title' => 'subtitle',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
          Know::create(array(
            'id' => 2,
            'title' => 'main',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
    }
}

class GeneticTableSeeder extends Seeder {
    public function run()
    {
        Genetic::create(array(
            'id' => 1,
            'title' => 'subtitle',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
          Genetic::create(array(
            'id' => 2,
            'title' => 'main',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
    }
}

class FormTableSeeder extends Seeder {
    public function run()
    {
        Form::create(array(
            'id' => 1,
            'title' => 'subtitle',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
          Form::create(array(
            'id' => 2,
            'title' => 'main',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
    }
}

class DirectionTableSeeder extends Seeder {
    public function run()
    {
        Direction::create(array(
            'id' => 1,
            'title' => 'subtitle',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
          Direction::create(array(
            'id' => 2,
            'title' => 'main',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
    }
}
 */
class HomeTableSeeder extends Seeder {
    public function run()
    {
        Home::create(array(
            'id' => 1,
            'title' => 'subtitle',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
          Home::create(array(
            'id' => 2,
            'title' => 'address',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
          Home::create(array(
            'id' => 3,
            'title' => 'hours',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
         Home::create(array(
            'id' => 4,
            'title' => 'contact',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
         Home::create(array(
            'id' => 5,
            'title' => 'first_featurette',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
         Home::create(array(
            'id' => 6,
            'title' => 'first_body',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
         Home::create(array(
            'id' => 7,
            'title' => 'second_featurette',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
         Home::create(array(
            'id' => 8,
            'title' => 'second_body',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
         Home::create(array(
            'id' => 9,
            'title' => 'third_featurette',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
         Home::create(array(
            'id' => 10,
            'title' => 'third_body',
            'body' => 'default',
            'created_at' => new DateTime,
            'updated_at' => new DateTime

        ));
    }
}
