<?php
    class SessionsController extends BaseController {
        public function create()
        {
            return View::make('sessions.create');
        }

        public function store()
        {
            // Validate

            $input = Input::all();

            $attempt = Auth::attempt([
                'email' => $input['email'],
                'password' => $input['password']
            ]);

            if ($attempt) return Redirect::intended('/');

            return Redirect::back();
            }

        public function destroy()
        {
            Auth::logout();

            return Redirect::home();
        }


    }
