<?php

    class HomeController extends \BaseController {

        public function update()
        {
            $input = Input::all();
            DB::update('update home set body = ? where id=1', array($input['home_subtitle']));
            DB::update('update home set body = ? where id=2', array($input['home_address']));
            DB::update('update home set body = ? where id=3', array($input['home_hours']));
            DB::update('update home set body = ? where id=4', array($input['home_contact']));
            DB::update('update home set body = ? where id=5', array($input['home_first_featurette']));
            DB::update('update home set body = ? where id=6', array($input['home_first_body']));
            DB::update('update home set body = ? where id=7', array($input['home_second_featurette']));
            DB::update('update home set body = ? where id=8', array($input['home_second_body']));
            DB::update('update home set body = ? where id=9', array($input['home_third_featurette']));
            DB::update('update home set body = ? where id=10', array($input['home_third_body']));
            return Redirect::back();
        }

    }



