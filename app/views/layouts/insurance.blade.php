@extends('layouts.master')

@section('content')

<script>
    $("li:has(a[href='insurance and programs'])").addClass("active");
</script>

    <div class="jumbotron">
        <h1>Insurance & Programs</h1>
            <p>{{ $content[0]->body }}</p>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>{{ $content[1]->body }}</p>
          </p>
        </div>
    </div>
@stop
