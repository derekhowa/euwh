@extends ('layouts.master')

@section ('content')

<script>
    $("li:has(a[href='/'])").addClass("active");
</script>

    <div class="jumbotron container">
        <h1>Eastern Utah Womens Health</h1>
        <p class="lead">{{ $content[0]->body }}</p>
      </div>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->
    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
          <img class="img-circle" src="{{ URL::to('img/directions1.jpeg') }}" alt="Generic placeholder image">
          <h2>Address:</h2>
            <p>{{ $content[1]->body }}</p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4 col-md-4 col-sm-4">
          <img class="img-circle" src="{{ URL::to('img/directions2.jpeg') }}" alt="Generic placeholder image">
          <h2>Hours of Operation:</h2>
            <p> {{ $content[2]->body }}</p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4 col-md-4 col-sm-4">
          <img class="img-circle" src="{{ URL::to('img/directions3.jpeg') }}" alt="Generic placeholder image">
          <h2>Contact:</h2>
            <p>{{ $content[3]->body }}</p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->


      <!-- START THE FEATURETTES -->

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7 col-sm-7 col-xs-7">
          <h2 class="featurette-heading">{{ $content[4]->body }} <!--<span class="text-muted">It'll blow your mind.</span>--></h2>
          <p class="lead">{{ $content[5]->body }}</p>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-5">
          <img class="featurette-image img-responsive img-circle" src="{{ URL::to('img/danielle.jpeg') }}" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-5 col-sm-5 col-xs-5">
          <img class="featurette-image img-responsive img-circle" src="{{ URL::to('img/interior.jpeg') }}" alt="Generic placeholder image">
        </div>
        <div class="col-md-7 col-sm-7 col-xs-7">
          <h2 class="featurette-heading">{{ $content[6]->body }}<!--<span class="text-muted">See for yourself.</span>--></h2>
          <p class="lead">{{ $content[7]->body }}</p>
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7 col-sm-7 col-xs-7">
          <h2 class="featurette-heading">{{ $content[8]->body }}<!-- <span class="text-muted">Checkmate.</span>--></h2>
          <p class="lead">{{ $content[9]->body }}</p>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-5">
          <img class="featurette-image img-responsive img-circle" src="{{ URL::to('img/sign.jpeg') }}" alt="Generic placeholder image">

        </div>
      </div>

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->
    </div>
@stop
