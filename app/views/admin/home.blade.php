@extends('admin.default')

@section('content')
<link rel="stylesheet" type="text/css" href="/css/bootstrap-wysihtml5.css"></link>
<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"></link>
<script src="/js/wysihtml5-0.3.0.js"></script>
<script src="/js/jquery-1.7.2.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-wysihtml5.js"></script>
<style>
textarea {
    width: 100%;
}
</style>
<div>
    <h1> Home </h1>
       {{ Form::open(array('action' => 'HomeController@update', 'method' => 'put')) }}
    <ul>
        <li>
            {{ Form::label('home_subtitle', 'Subtitle:') }}
            {{ Form::textarea('home_subtitle', $content[0]->body ) }}
        </li>

        <li>
            {{ Form::label('home_address', 'Address:') }}
            {{ Form::textarea('home_address', $content[1]->body ) }}
        </li>
            <li>
            {{ Form::label('home_hours', 'Hours:') }}
            {{ Form::textarea('home_hours', $content[2]->body ) }}
        </li>
       <li>
            {{ Form::label('home_contact', 'Contact:') }}
            {{ Form::textarea('home_contact', $content[3]->body ) }}
        </li>
       <li>
            {{ Form::label('home_first_featurette', 'First Featurette:') }}
            {{ Form::textarea('home_first_featurette', $content[4]->body ) }}
        </li>
       <li>
            {{ Form::label('home_first_body', 'First Featurette Body:') }}
            {{ Form::textarea('home_first_body', $content[5]->body ) }}
        </li>
       <li>
            {{ Form::label('home_second_featurette', 'Second Featurette:') }}
            {{ Form::textarea('home_second_featurette', $content[6]->body ) }}
        </li>
             <li>
            {{ Form::label('home_second_body', 'Second Featurette Body:') }}
            {{ Form::textarea('home_second_body', $content[7]->body ) }}
        </li>
       <li>
            {{ Form::label('home_third_featurette', 'Third Featurette:') }}
            {{ Form::textarea('home_third_featurette', $content[8]->body ) }}
        </li>
       <li>
            {{ Form::label('home_third_body', 'Third Featurette Body:') }}
            {{ Form::textarea('home_third_body', $content[9]->body ) }}
        </li>
        <li>
            {{ Form::submit() }}
        </li>
    </ul>
{{ Form::close() }}
</div>
<script type="text/javascript">
$('#home_subtitle, #home_address, #home_hours, #home_contact, #home_first_featurette, #home_first_body, #home_second_featurette, #home_second_body, #home_third_featurette, #home_third_body').wysihtml5();
</script>
@stop
