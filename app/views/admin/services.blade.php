@extends('admin.default')

@section('content')
<link rel="stylesheet" type="text/css" href="/css/bootstrap-wysihtml5.css"></link>
<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"></link>
<script src="/js/wysihtml5-0.3.0.js"></script>
<script src="/js/jquery-1.7.2.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-wysihtml5.js"></script>
<style>
textarea {
    width: 100%;
}
</style>

<div>
    <h1> Services </h1>
       {{ Form::open(array('action' => 'ServicesController@update', 'method' => 'put')) }}
    <ul>
        <li>
            {{ Form::label('services_subtitle', 'Subtitle:') }}
            {{ Form::textarea('services_subtitle', $content[0]->body ) }}
        </li>

        <li>
            {{ Form::label('services_main', 'Main Body:') }}
            {{ Form::textarea('services_main', $content[1]->body ) }}
        </li>

        <li>
            {{ Form::submit() }}
        </li>
    </ul>
{{ Form::close() }}
</div>
<script type="text/javascript">
$('#services_subtitle, #services_main').wysihtml5();
</script>
@stop

