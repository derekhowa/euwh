@extends('admin.default')

@section('content')
<link rel="stylesheet" type="text/css" href="/css/bootstrap-wysihtml5.css"></link>
<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"></link>
<script src="/js/wysihtml5-0.3.0.js"></script>
<script src="/js/jquery-1.7.2.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-wysihtml5.js"></script>
<style>
textarea {
    width: 100%;
}
</style>
<div>
    <h1> Programs </h1>
       {{ Form::open(array('action' => 'ProgramsController@update', 'method' => 'put')) }}
    <ul>
        <li>
            {{ Form::label('programs_subtitle', 'Subtitle:') }}
            {{ Form::textarea('programs_subtitle', $content[0]->body ) }}
        </li>

        <li>
            {{ Form::label('programs_main', 'Main Body:') }}
            {{ Form::textarea('programs_main', $content[1]->body ) }}
        </li>

        <li>
            {{ Form::submit() }}
        </li>
    </ul>
{{ Form::close() }}
<script type="text/javascript">
$('#programs_subtitle, #programs_main').wysihtml5();
</script>
</div>

@stop

