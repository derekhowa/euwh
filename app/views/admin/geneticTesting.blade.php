@extends('admin.default')

@section('content')
<link rel="stylesheet" type="text/css" href="/css/bootstrap-wysihtml5.css"></link>
<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"></link>
<script src="/js/wysihtml5-0.3.0.js"></script>
<script src="/js/jquery-1.7.2.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-wysihtml5.js"></script>
<style>
textarea {
    width: 100%;
}
</style>
<div>
    <h1> Genetics </h1>
       {{ Form::open(array('action' => 'GeneticsController@update', 'method' => 'put')) }}
    <ul>
        <li>
            {{ Form::label('genetics_subtitle', 'Subtitle:') }}
            {{ Form::textarea('genetics_subtitle', $content[0]->body ) }}
        </li>

        <li>
            {{ Form::label('genetics_main', 'Main Body:') }}
            {{ Form::textarea('genetics_main', $content[1]->body ) }}
        </li>

        <li>
            {{ Form::submit() }}
        </li>
    </ul>
{{ Form::close() }}
</div>
<script type="text/javascript">
$('#genetics_subtitle, #genetics_main').wysihtml5();
</script>
@stop

