<?php

Route::get('admin', function()
{
    return View::make('admin.index');
})->before('auth');

Route::get('login', 'SessionsController@create');
Route::get('logout', 'SessionsController@destroy');

// Route Resources

Route::resource('sessions', 'SessionsController', ['only' => ['index', 'create', 'destroy', 'store', 'update']]);
Route::resource('admin/services', 'ServicesController', ['only' =>['update']]);
Route::resource('admin/directions', 'DirectionsController', ['only' =>['update']]);
Route::resource('admin/programs', 'ProgramsController', ['only' =>['update']]);
Route::resource('admin/genetics', 'GeneticsController', ['only' =>['update']]);
Route::resource('admin/forms', 'FormsController', ['only' =>['update']]);
Route::resource('admin/directions', 'DirectionsController', ['only' =>['update']]);
Route::resource('admin/know', 'KnowController', ['only' =>['update']]);
Route::resource('admin/home', 'HomeController', ['only' =>['update']]);

Route::get('/', ['as' => 'home', function()
{
    $content = DB::table('home')->get();
	return View::make('layouts.home', compact('content'));
}]);

Route::get('services', function()
{
    $content = DB::table('services')->get();
    return View::make('layouts.services', compact('content'));
});

Route::get('insurance and programs', function()
{
    $content = DB::table('programs')->get();
    return View::make('layouts.insurance', compact('content'));
});

Route::get('genetic testing', function()
{
    $content = DB::table('genetics')->get();
    return View::make('layouts.geneticTesting', compact('content'));
});

Route::get('forms', function()
{
    $content = DB::table('forms')->get();
    return View::make('layouts.forms', compact('content'));
});

Route::get('get to know us', function()
{
    $content = DB::table('know')->get();
    return View::make('layouts.getToKnowUs', compact('content'));
});

Route::get('directions', function()
{
    $content = DB::table('directions')->get();
    return View::make('layouts.directions', compact('content'));
});

// ADMIN

Route::get('/admin/home', function()
{
    $content = DB::table('home')->get();
    return View::make('admin.home', compact('content'));
})->before('auth');

Route::get('/admin/services', function()
{
    $content = DB::table('services')->get();
    return View::make('admin.services', compact('content'));
})->before('auth');

Route::get('/admin/insurance and programs', function()
{
    $content = DB::table('programs')->get();
    return View::make('admin.programs', compact('content'));
})->before('auth');

Route::get('/admin/genetic testing', function()
{
    $content = DB::table('genetics')->get();
    return View::make('admin.geneticTesting', compact('content'));
})->before('auth');

Route::get('/admin/forms', function()
{
    $content = DB::table('forms')->get();
    return View::make('admin.forms', compact('content'));
})->before('auth');

Route::get('/admin/get to know us', function()
{
    $content = DB::table('know')->get();
    return View::make('admin.know', compact('content'));
})->before('auth');

Route::get('/admin/directions', function()
{
    $content = DB::table('directions')->get();
    return View::make('admin.directions', compact('content'));
})->before('auth');


